import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = 'http://localhost:8000/backend/countrystats/';

  constructor(
    private httpClient: HttpClient
  ) { }

  getCountryStat() {
    return this.httpClient.get(this.baseUrl);
  }
}
