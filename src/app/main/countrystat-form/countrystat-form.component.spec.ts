import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrystatFormComponent } from './countrystat-form.component';

describe('CountrystatFormComponent', () => {
  let component: CountrystatFormComponent;
  let fixture: ComponentFixture<CountrystatFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrystatFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrystatFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
