import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrystatListComponent } from './countrystat-list.component';

describe('CountrystatListComponent', () => {
  let component: CountrystatListComponent;
  let fixture: ComponentFixture<CountrystatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrystatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrystatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
