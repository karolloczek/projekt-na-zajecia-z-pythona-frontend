import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-countrystat-list',
  templateUrl: './countrystat-list.component.html',
  styleUrls: ['./countrystat-list.component.css']
})
export class CountrystatListComponent implements OnInit {

  public countryStats;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.getCountryStat().subscribe(
      data => {
        this.countryStats = data;
      },
      error => console.log(error)
    );
  }

}
