import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrystatDetailsComponent } from './countrystat-details.component';

describe('CountrystatDetailsComponent', () => {
  let component: CountrystatDetailsComponent;
  let fixture: ComponentFixture<CountrystatDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrystatDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrystatDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
