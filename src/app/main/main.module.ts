import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ApiService } from '../api.service';

import { MainComponent } from './main.component';
import { CountrystatListComponent } from './countrystat-list/countrystat-list.component';
import { CountrystatDetailsComponent } from './countrystat-details/countrystat-details.component';
import { CountrystatFormComponent } from './countrystat-form/countrystat-form.component';

const routes: Routes = [
  {path: 'countrysstat', component: MainComponent}
];

@NgModule({
  declarations: [
    MainComponent,
    CountrystatListComponent,
    CountrystatDetailsComponent,
    CountrystatFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    ApiService
  ]
})
export class MainModule { }
